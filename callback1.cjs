function board_information(id,boards,callback){
    try{
        const result=boards.filter((each)=>
            each.id==id
        )
        setTimeout(()=>{
            callback(null,result)
        },2000)
    }
    catch(err){
        console.error(err)
    }
}
module.exports=board_information
