function card_information(id,cards,callback){
    try{
        const result=Object.entries(cards).filter((each)=>each[0]==id)
        
        setTimeout(()=>{
            callback(null,Object.fromEntries(result))
        },2000)
    }
    catch(err){
        console.error(err)
    }
}

module.exports=card_information