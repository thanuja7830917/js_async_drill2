function list_information(id,list,callback){
    try{
        const result=Object.entries(list).filter((each)=>each[0]==id)
        
        setTimeout(()=>{
            callback(null,Object.fromEntries(result))
        })
    }
    catch(err){
        console.error(err)
    }
}
module.exports=list_information