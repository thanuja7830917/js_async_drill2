const board_information=require("./callback1.cjs")

const boards=require("./data/boards_2.json")

const lists=require("./data/lists_1.json")

const cards=require("./data/cards_2.json")

const list_information = require("./callback2.cjs")

const card_information = require("./callback3.cjs")

function previous_functions(name){
    try{
        const board_id=boards.filter((each)=>each.name==name)
       
        board_information(board_id[0].id,boards,(err,data)=>{
            if(err){
                console.error(err)
            }
            else{
                console.log(data)
                list_information(data[0].id ,lists,(err,data)=>{
                    if(err){
                        console.error(err)
                    }
                    else{
                        console.log(data)
                        
                        const arr_data=Object.entries(data)

                        const mind_id=arr_data[0][1].filter((each)=>each.name=="Mind")

                        card_information(mind_id[0].id,cards,(err,data)=>{
                            if(err){
                                console.error(err)
                            }
                            else{
                                console.log(data)
                            }
                        })
                    }
                })
            }
        })
    }
    catch(err){
        console.error(err)
    }
}
module.exports=previous_functions
